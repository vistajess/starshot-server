var express = require('express');
var cors = require('cors')
var app = express();
var port = process.env.PORT || 8080;
app.use(cors())

app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});

app.get('/data', function (req, res) {
    var mockData = [
        [{ name: "Comp A", employees: 26 }, { name: "Comp B", employees: 28 }, { name: "Comp C", employees: 30 }, { name: "Comp D", employees: 2 }, { name: "Comp E", employees: 6 } ],
        [{ name: "Comp A", employees: 15 }, { name: "Comp B", employees: 41 }, { name: "Comp C", employees: 11 }, { name: "Comp D", employees: 12 }, { name: "Comp E", employees: 62}],
        [{ name: "Comp A", employees: 5 }, { name: "Comp B", employees: 12 }, { name: "Comp C", employees: 22 }, { name: "Comp D", employees: 10 }, { name: "Comp E", employees: 9}],
        []
    ]
    var randomIndex = Math.floor(Math.random() * 3) + 1;
    console.log(randomIndex);
    res.status(200).send({ results : mockData[randomIndex] })
});

var server = app.listen(port, function () {
   var host = server.address().address
   var port = server.address().port
   
   console.log("App listening at http://%s:%s", host, port)
})